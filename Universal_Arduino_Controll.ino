#define max_arguments 4
#define debug false
#define nDPins 14
#define nAPins 6
#define nPins nDPins + nAPins
#define argLength 10

#include "Wire.h"


char *inputBuff = (char*) malloc(30);
char **arguments = (char**) malloc(max_arguments);
char Read;
uint8_t index, arg_index, argument;
int val1, val2, val3;
bool doing_command = false, doing_arg = false;

/*struct function {
  char *name;
  char *arguments;
};*/

struct pin {
  uint8_t mode = 0;
  uint8_t value = 0;
  char *name = (char*) malloc(4);
};

struct pin pins[nPins];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println(F("Hello, booting up"));

  for (int i = 0; i < max_arguments; i++) {
    arguments[i] = (char*) malloc(argLength);
    arguments[i][0] = '\0';
  }

  for (int i = 0; i < nPins; i++) {
    if (i < nDPins) {
      sprintf(pins[i].name, "D%d", i); // 0 ASCII character is 48
    } else {
      sprintf(pins[i].name, "A%d", i - nDPins); // 0 ASCII character is 48
    }
  }
}

void loop() {
  if (Serial.available()) {
    Read = Serial.read();
    if (doing_arg) {
      // Moving on to next argument
      if (Read == ',' || Read == ')') {
        arguments[argument][arg_index] = '\0';
        #if (debug)
        Serial.println(arguments[argument]);
        #endif
        argument ++;
        arg_index = 0;
        
        //If the argument is closed
        if (Read == ')') {
          doing_arg = false;
          #if (debug)
          Serial.println(F("Closing bracket"));
          #endif
        }
      }
      // Delete spaces
      else if (Read == ' ') {}
      //Should not occure
      else if (Read == '\n') {
        faultCommand(0);
      }
      else {
        arguments[argument][arg_index] = Read;
        arg_index++;
      }
    } else {
      if (Read == '(') {
        argument = 0;
        doing_arg = true;
        #if (debug)
        Serial.println(F("\nOpening bracket"));
        #endif
      } else if (doing_command && Read == '\n') {
        inputBuff[index] = '\0';
        index = 0;
        exeCommand();
        doing_command = false;
      } else if (doing_command) {
        inputBuff[index] = Read;
        #if (debug)
        Serial.write(Read);
        #endif
        index++;
      } else {
        doing_command = true;
        inputBuff[index] = Read;
        #if (debug)
        Serial.println(F("Starting command"));
        Serial.write(Read);
        #endif
        index++;
      }
    }
  }
}

void exeCommand () {
  #if (debug)
  Serial.println(F("There we go:"));
  #endif
  
  // Executing [with arguments ['arg1'][, 'arg2'][, 'arg3'][, 'arg4']...] where [] means optional block
  Serial.print(F("Executing `"));
  Serial.print(inputBuff);
  
  // Print only the arguments that were put in
  if (arguments[0][0])
  {
    Serial.print(F("` with arguments '"));
    for (int i = 0; i < argument; i++) {
      // Comma is only needed when !first arg
      if (i) { Serial.print(F("', '")); }
      Serial.print(arguments[i]);
    }
    // End string with a '
    Serial.write('\'');
  }
  Serial.write('\n');

  // Select correct function
  if (strcmp(inputBuff, "digitalWrite") == 0) {
    f_digitalWrite();
  } else if (strcmp(inputBuff, "digitalRead") == 0) {
    f_digitalRead();
  } else if (strcmp(inputBuff, "analogWrite") == 0) {
    f_analogWrite();
  } else if (strcmp(inputBuff, "analogRead") == 0) {
    f_analogRead();
  } else if (strcmp(inputBuff, "status") == 0) {
    f_status();
  } else if (strcmp(inputBuff, "I2C_scan") == 0) {
    f_I2C_scan();
  } else if (strcmp(inputBuff, "command") == 0) {
    faultCommand (0);
  } else {
    Serial.println(F("No command was found"));
  }

  Serial.write('\n');
}

void faultCommand (uint8_t code) {
  switch (code) {
    case 0:
      Serial.println(F("Command was not finished propperly."));
      break;
    case 1:
      Serial.println(F("Error parsing your argument"));
      break;
    case 2:
      Serial.println(F("Only digital ports are alowed"));
      break;
    default:
      Serial.println(F("Unknow error"));
  }
}

void getPin(uint8_t argNumber) {
  //For referencing analog pins
  if (arguments[argNumber][0] == 'A') {
    arguments[argNumber][0] = '0';
    #if (debug)
    Serial.println(F("Detected Analog"));
    #endif
    val3 = nDPins;
  } else { 
    val3 = 0;
    if (arguments[argNumber][0] == 'D') {
      arguments[argNumber][0] = '0';
    }
  }
  
  //Read pin to be used
  if(!sscanf(arguments[argNumber], "%d", &val1)) { faultCommand(1); }
  //Add compensation for the analog pin
  val1 += val3;
}

void f_status () {
  for (int i = 0; i < nPins; i++) {
    if (pins[i].mode) {
      
      Serial.print(pins[i].name);
      Serial.print(F("\tMode = "));
      //Serial.print(pins[i].mode);
      switch (pins[i].mode) {
        case 1:
          Serial.print(F("Digital write"));
          break;
        case 2:
          Serial.print(F("Digital read"));
          break;
        case 3:
          Serial.print(F("Analog write"));
          break;
        case 4:
          Serial.print(F("Analog read"));
          break;
        default:
          faultCommand(255);
      }
      Serial.print(F("\tValue = "));
      Serial.println(pins[i].value);
    }
  }
}

void f_digitalWrite () {
  //Get the pin number from the input arguments
  getPin(0);
  
  pinMode(val1, OUTPUT);
  pins[val1].mode = 1;
  
  if (strcmp(arguments[1], "HIGH") == 0) {
    pins[val1].value = 1;
    digitalWrite(val1, HIGH);
  } else if (strcmp(arguments[1], "LOW") == 0) {
    pins[val1].value = 0;
    digitalWrite(val1, LOW);
  } else { faultCommand(1); }
}

void f_digitalRead () {
  //Get the pin number from the input arguments
  getPin(0);
  
  pinMode(val1, INPUT);
  pins[val1].mode = 2;
  pins[val1].value = digitalRead(val1);

  Serial.print(F("Value on pin "));
  Serial.print(pins[val1].name); // Pin name
  Serial.print(F(" is "));
  if (pins[val1].value) {
    Serial.println(F("HIGH"));
  } else {
    Serial.println(F("LOW"));
  }
}

void f_analogWrite () {
  if (arguments[0][0] == 'D') {
    arguments[0][0] = '0';
  } else if (isAlpha(arguments[0][0])) {
    faultCommand(3);
  }
  // Read pin number
  if(!sscanf(arguments[0], "%d", &val1)) { faultCommand(1); }
  if(val1 > nDPins) { faultCommand(3); }
  
  // Read value to be written
  if(!sscanf(arguments[1], "%d", &val2)) { faultCommand(1); }
  
  pins[val1].mode = 3;
  pins[val1].value = val2;
  pinMode(val1, OUTPUT);
  analogWrite(val1, val2);
  
  #if debug
    Serial.print(val1);
    Serial.write('\t');
    Serial.println(val2);
  #endif
  
  Serial.print(F("Pin "));
  Serial.print(pins[val1].name);
  Serial.print(F(" to analog value "));
  Serial.print(pins[val1].value);
}

void f_analogRead () {
  if (arguments[0][0] == 'A') {
    arguments[0][0] = '0';
  } else if (isAlpha(arguments[0][0])) {
    Serial.println(F("Only analog ports are alowed"));
  }

  // Read pin number
  if(!sscanf(arguments[0], "%d", &val1)) { faultCommand(1); }
  if (val1 < nDPins) { val1 += nDPins; }

  // Put pinmode back to input, because it could have been an ouput at first
  pinMode(val1, INPUT);

  pins[val1].value = analogRead(val1);
  pins[val1].mode = 4;

  Serial.print(F("Value on pin "));
  Serial.print(pins[val1].name);
  Serial.print(F(" is "));
  Serial.println(pins[val1].value);
}

void f_I2C_scan () {
  byte error;
 
  val1 = 0;
  for(int i = 1; i < 127; i++ )
  {
    // The i2c_scanner uses the return value of the Write.endTransmisstion to see if a device did acknowledge to the address.
    Wire.beginTransmission(i);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (i < 16)
        Serial.print("0");
      Serial.print(i, HEX);
      Serial.println(F("  !"));
 
      val1++;
    }
    else if (error == 4)
    {
      Serial.print("Unknow error at address 0x");
      if (i < 16)
        Serial.print("0");
      Serial.println(i, HEX);
    }    
  }
  if (val1 == 0) {
    Serial.println(F("No I2C devices found\n"));
  }
}

